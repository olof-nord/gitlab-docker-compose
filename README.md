# gitlab-docker-compose

GitLab, GitLab CI and GitLab Pages running with traefik routing with a Docker-Compose.

## Start

TO start all instances, use `docker-compose up --detach`.

## Setup

The GitLab CI runner needs to be added to the GitLab instance using the command below.

To find out the value for the registration token, go to the Shared Runners section in the GitLab Admin page.

```shell
gitlab-runner register \
  --non-interactive \
  --url "http://gitlab" \
  --registration-token "$VALUE_HERE" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner" \
  --run-untagged="true" \
  --locked="false" \
  --docker-network-mode="public"
```

To open the GitLab runner shell, use `docker-compose exec gitlab-runner bash`.

The runner configuration file is stored at `/etc/gitlab-runner/config.toml`

## Addresses

The GitLab UI should be accessible at http://gitlab.127.0.0.1.nip.io/.

## Resources

- [byeCloud: GitLab with Docker and Traefik](https://www.davd.io/byecloud-gitlab-with-docker-and-traefik/)
- [Howto setup gitlab](https://github.com/streuspeicher/howto-setup-gitlab)
